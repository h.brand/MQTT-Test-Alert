#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
A very simple MQTT subscriber script.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Acknowledgements
----------------
- The MQTT part is inpired by http://www.steves-internet-guide.com
"""

import paho.mqtt.client as mqtt


def on_message(client, userdata, message):
    """Handle received messge."""
    msg = str(message.payload.decode("utf-8"))
    print("message received: ", msg)
    print("message topic: ", message.topic)


def on_connect(client, userdata, flags, rc):
    """Perform actions after client connected to MQTT Broker."""
    client.subscribe('/MQTT/MQTTActor')


BROKER_ADDRESS = "localhost"

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(BROKER_ADDRESS)

print("Connected to MQTT Broker: " + BROKER_ADDRESS)

client.loop_forever()
