#! c:\python34\python3
#!/usr/bin/env python
##demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose
##If you like and use this code you can
##buy me a drink here https://www.paypal.me/StepenCope
"""
Creates multiple Connections to a broker 
and sends and receives messages. Support SSL and Normal connections
Notice the use of an array to store the client objects
Shows number of thread used
Uses a single thread to manually run the client loop.
The function Multi_loop is at top of script
Notice increasing the number of clients doesn't increase the
number of threads
"""
import paho.mqtt.client as mqtt
import time
import sys
import threading
import logging

run_flag=True
message="test message"
broker="192.168.1.41"
topic="test_topic"
port=1883
nclients=2 #adjust accordingly
#Notice increasing the number of clients doesn't increase the
#number of threads
clients=[]
out_queue=[] #use simple array to get printed messages in some form of order
##########
def multi_loop(nclients):
   global run_flag
   while run_flag:
      for i in range(nclients):
         client=clients[i]
         client.loop(0.01)

#########

def on_log(client, userdata, level, buf):
   print(buf)
def on_message(client, userdata, message):
   time.sleep(1)
   msg="message received",str(message.payload.decode("utf-8"))
   #print(msg)
   out_queue.append(msg)
def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True #set flag   
        client.subscribe(topic)
    else:
        print("Bad connection Returned code=",rc)
        exit()  
def on_disconnect(client, userdata, rc):
   pass
   #print("client disconnected ok")
def on_publish(client, userdata, mid):
   time.sleep(1)
   print("In on_pub callback mid= "  ,mid)


def Create_connections():
   for i in range(nclients):
      cname="client"+str(i)
      t=int(time.time())
      client_id =cname+str(t) #create unique client_id
      client = mqtt.Client(client_id)             #create new instance

      try:
         client.connect(broker,port)           #establish connection
      except:
         print("Connection Fialed to broker ",broker)
         continue
      clients.append(client)
      #client.on_log=on_log #this gives getailed logging
      client.on_connect = on_connect
      client.on_disconnect = on_disconnect
      #client.on_publish = on_publish
      client.on_message = on_message
      while not client.connected_flag:
         client.loop(0.01) #check for messages
         time.sleep(0.05)


mqtt.Client.connected_flag=False #create flag in class
no_threads=threading.active_count()
print("current threads =",no_threads)
print("Creating  Connections ",nclients," clients")

   
Create_connections()
t = threading.Thread(target=multi_loop,args=(nclients,))#start multi loop
t.start()
print("All clients connected ")
time.sleep(5)
#
count =0
no_threads=threading.active_count()
print("current threads =",no_threads)
print("Publishing ")

#
#
try:
   while run_flag: 
      i=0
      for i in range(nclients):
         client=clients[i]
         counter=str(count).rjust(6,"0")
         msg="client "+ str(i) +  " "+counter+"XXXXXX "+message
         if client.connected_flag:
            client.publish(topic,msg)
            time.sleep(0.1)
            print("publishing client "+ str(i))
         i+=1
      time.sleep(10)#now print messages
      print("queue length=",len(out_queue))
      for x in range(len(out_queue)):
         print(out_queue.pop())
      count+=1
      #time.sleep(5)#wait
except KeyboardInterrupt:
   print("interrupted  by keyboard")
   for client in clients:
      client.disconnect()
   run_flag=False


#allow time for allthreads to stop before existing
time.sleep(5)


