#! c:\python34\python.exe
#!/usr/bin/env python
##demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose
"""
Simple publish and sbscribe. See the callback script for code to get the message from the
on_message callback into the main script. Notice I use a wildcard to subscribe so I receive message
sent on multiple topics
"""
broker="test.mosquitto.org"
broker="192.168.1.41"
#broker="localhost"
import paho.mqtt.client as mqtt  #import the client1
import time
def on_log(client, userdata, level, buf):
        print("log: "+buf)
def on_connect(client, userdata, flags, rc):
    if rc==0:
        print("connected OK")
    else:
        print("Bad connection Returned code=",rc)
def on_disconnect(client, userdata, flags, rc=0):
        print("DisConnected result code "+str(rc))

def on_message(client,userdata,msg):
        topic=msg.topic
        m_decode=str(msg.payload.decode("utf-8","ignore"))
        print("message received",m_decode)

client = mqtt.Client("python1")#create new instance

client.on_connect=on_connect  #bind call back function
client.on_disconnect=on_disconnect
#client.on_log=on_log
client.on_message=on_message
print("Connecting to broker ",broker)

client.connect(broker)      #connect to broker
client.loop_start()  #Start loop
client.subscribe("house/#")
client.publish("house/sensor1","my first message")
time.sleep(3)
client.publish("house/sensors/s1","my second message")
time.sleep(4)
client.loop_stop()    #Stop loop 
client.disconnect() # disconnect



