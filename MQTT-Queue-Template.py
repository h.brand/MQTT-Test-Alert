#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A template to be used as starting point for new MQTT projects.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL.

Acknowledgements
----------------
- The MQTT part is inpired by http://www.steves-internet-guide.com
"""

import datetime
import os
import platform
import queue
import re
import sys
import threading
import time
import paho.mqtt.client as mqtt

broker = "localhost"
hostname = platform.node()
client_name = hostname + "_" + re.split(".py", os.path.basename(__file__))[0]
date_time = re.split(" ", str(datetime.datetime.now()))
client_id = client_name  # + "_" + date_time[0] + "_" + date_time[1]
topic_command = client_id + "/Command"
topic_status = client_id + "/Status"

q = queue.Queue()


def on_log(client, userdata, level, buf):
    """Log buffer if callback is assigned."""
    print("log: " + buf)
    pass


def on_connect(client, userdata, flags, rc):
    """
    Handle broker connected callback.

    Publish all topic once.
    Subscribe to desired topics.
    """
    if rc == 0:
        client.connected_flag = True
        print("Connected OK")
        publish_all(client)
        subscribe_all(client)
    else:
        print("Bad connection, returned code =", rc)
    pass


def on_disconnect(client, userdata, flags, rc=0):
    """Handle broker disconnected callback."""
    # client.connected_flag = False
    print("Disconnected result code =" + str(rc))
    pass


def on_publish(client, userdata, mid):
    """Handle publish callback."""
    # print("Client published message ID =", mid)
    pass


def on_message(client, userdata, msg):
    """
    Handle message received callback.

    Decode received message data and insert into command processor queue.
    """
    topic = msg.topic
    m_decode = str(msg.payload.decode("utf-8", "ignore"))
    # print("Message received. Topic:", topic, "Payload:", m_decode)
    q.put((topic, m_decode))
    pass


def on_subscribe(client, userdata, mid, granted_qos):
    """Handle subscribed callback."""
    # print("Client subscribed message ID =", mid, "with qos =", granted_qos)
    pass


def on_unsubscribe(client, userdata, mid):
    """Handle unsubscribed callback."""
    # print("Client unsubscribed message ID =", mid)
    pass


def publish_all(client):
    """Publish all topics."""
    rc, mid = client.publish(client_id, "connected")
    print("Publishing: 'connected' returned rc =", rc, "mid = ", mid)
    rc, mid = client.publish(topic_status, 0)
    print("Publishing: '", topic_status, "' returned rc =", rc, "mid = ", mid)
    rc, mid = client.publish(topic_command, "Write your command here.")
    print("Publishing: '", topic_command, "' returned rc =", rc, "mid = ", mid)
    pass


def subscribe_all(client):
    """Subscribe to all topics."""
    rc, mid = client.subscribe(topic_command)
    print("Subscribing to: '", topic_command, "'returned rc =", rc, "mid = ", mid)
    pass


def unsubscribe_all(client):
    """Unsubscribe from all topics."""
    rc, mid = client.unsubscribe(topic_command)
    print("Unsubscribing to: '", topic_command, "'returned rc =", rc, "mid = ", mid)
    pass


def command_processor():
    """
    Implement command processing here.

    Get decoded message from queue and process.
    Example: Handle topic 'Command'.
    """
    while True:
        received = q.get()
        topics = re.split("/", received[0])
        data = received[1]
        print("Command processing:", topics[1])
        if topics[1] == "Command":
            print(topics[1], data)
        else:
            print("Received unkown:", received[0])
        print("Command processing done.")
        q.task_done()
        pass


def periodic_processor(client, ii):
    """Perform periodic actions."""
    client.publish(topic_status, ii)
    pass


if __name__ == '__main__':
    print(os.path.basename(__file__), "\n\
    Copyright 2021 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
    Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
    eMail: H.Brand@gsi.de\n\
    Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\
    Published under EUPL.")
    # Insert you main code here
    try:
        # client = mqtt.Client(client_id="", clean_session=True, userdata=None, protocol=MQTTv311, transport="tcp")
        client = mqtt.Client(client_id, clean_session=True)
        client.connected_flag = False
        client.will_set(client_id, "Offline", 1, False)
        # bind call back function
        client.on_connect = on_connect
        client.on_disconnect = on_disconnect
        # client.on_log=on_log
        client.on_publish = on_publish
        client.on_message = on_message
        client.on_subscribe = on_subscribe
        client.on_unsubscribe = on_unsubscribe
        print("Connecting client '", client_id, " 'to broker", broker)
        # turn-on the worker thread
        threading.Thread(target=command_processor, daemon=True).start()
        client.loop_start()
        client.connect(broker, port=1883, keepalive=60, bind_address="")
        while not client.connected_flag:
            print("Waiting for", broker, "...")
            time.sleep(1)
        time.sleep(3)
        print("Waiting for exception (Ctrl+c) ...")
        ii = 0
        while client.connected_flag:
            ii += 1
            periodic_processor(client, ii)
            # client.publish(topic_status, ii)
            time.sleep(1)
    except BaseException as e:
        print("Exception catched!", e)
        client.connected_flag = False
    finally:
        unsubscribe_all(client)
        q.join()
        print("Publishing: 'disconnected'")
        rc, mid = client.publish(client_id, "disconnected")
        print("Publishing: 'disconnected' returned rc =", rc, "mid = ", mid)
        print("Disonnecting from broker", broker)
        client.disconnect()
        time.sleep(1)
        print("Stopping message loop")
        client.loop_stop(force=False)
        print("Script execution stopped.")
        sys.exit()
