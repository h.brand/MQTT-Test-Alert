#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Read trend data from file recorded with mqtt-recorder.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL.
"""

import datetime
import json
import matplotlib.pyplot as plt
import numpy as np

# Opening record definition JSON file
with open('Topics-bool.json',) as fObj:
    topics_bool = json.load(fObj)['topics']
    # print(topics_bool)
    csv_data_type_bool = np.dtype(
        [
            ('Topic', np.compat.unicode, 100),
            ('Payload', np.bool_),
            ('QoS', np.int8),
            ('Retain', np.int8),
            ('Time', np.float64),
            ('dt', np.float64)
        ]
    )
    csv_data_bool = np.genfromtxt("Topics-Record.csv", dtype=csv_data_type_bool, delimiter=",")
    t_zero_bool = csv_data_bool['Time'][0]
with open('Topics-float64.json',) as fObj:
    topics_float = json.load(fObj)['topics']
    # print(topics_float)
    csv_data_type_float = np.dtype(
        [
            ('Topic', np.compat.unicode, 100),
            ('Payload', np.float64),
            ('QoS', np.int8), ('Retain', np.int8),
            ('Time', np.float64),
            ('dt', np.float64)
        ]
    )
    csv_data_float = np.genfromtxt("Topics-Record.csv", dtype=csv_data_type_float, delimiter=",")
    t_zero_float = csv_data_float['Time'][0]
t_zero = min(t_zero_bool, t_zero_float)
fig = plt.figure(figsize=(10, 5))
sub1 = fig.add_subplot(2, 1, 1)
sub2 = fig.add_subplot(2, 1, 2)
sub1.set_title('MQTT Recorded Analog Data (t0=' + datetime.datetime.utcfromtimestamp(t_zero).strftime("%d.%m.%y %H:%M:%S") + ')')
sub1.set_xlabel('Time / s')
sub1.set_ylabel('Value / V')
sub2.set_title('MQTT Recorded Digital Data (t0=' + datetime.datetime.utcfromtimestamp(t_zero).strftime("%d.%m.%y %H:%M:%S") + ')')
sub2.set_xlabel('Time / s')
sub2.set_ylabel('Value')
for topic in topics_float:
    # print('Topic:', topic)
    data_float = csv_data_float[csv_data_float['Topic'] == topic]
    data_f = data_float['Payload']
    t_f = (data_float['Time'] - t_zero)
    # print(t_f, data_f)
    sub1.plot(t_f, data_f, label=topic)
for topic in topics_bool:
    # print('Topic:', topic)
    data_bool = csv_data_bool[csv_data_bool['Topic'] == topic]
    data_b = data_bool['Payload']
    t_b = (data_bool['Time'] - t_zero)
    # print(t_b, data_b)
    sub2.step(t_b, data_b, where='post', label=topic)
    sub2.plot(t_b, data_b, 'o')
sub1.legend(loc='upper left')
sub2.legend(loc='upper left')
plt.tight_layout()
plt.show()
